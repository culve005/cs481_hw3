﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CS433_HW3
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MainPage : ContentPage
	{
		public MainPage ()
		{
			InitializeComponent ();

        }

        void OnAppearing(Object sender, System.EventArgs e)
        {
        }

        void OnDisappearing(Object sender, System.EventArgs e)
        {
        }
        
        private async void Serenity_Tapped(Object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Page1());
        }

        private async void Rogers_Tapped(Object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Page2());
        }

        private async void Falcon_Tapped(Object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Page3());
        }

    

    }
}