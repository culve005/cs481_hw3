﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CS433_HW3
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Page3 : ContentPage
	{
		public Page3 ()
		{
			InitializeComponent ();
		}

        void OnAppearing(Object sender, System.EventArgs e)
        {
            DisplayAlert("Millennium Falcon", "You have selected the ship Millennium Falcon! Please press OK to continue", "OK");
        }
        void Button_Clicked(Object sender, System.EventArgs e)
        {
            label.Text = "You've successfully joined the Millennium Falcon Crew";
        }
        void OnDisappearing(Object sender, System.EventArgs e)
        {
            DisplayAlert("Millennium Falcon", "You are leaving this page. Please press BYE to continue", "BYE");
        }
    }
}