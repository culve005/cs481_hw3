﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CS433_HW3
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Page1 : ContentPage
	{
		public Page1 ()
		{
			InitializeComponent ();
		}

        void OnAppearing(Object sender, System.EventArgs e)
        {
           DisplayAlert("Serenity", "You have selected the ship Serenity! Please press OK to continue", "OK");
        }
        void Button_Clicked(Object sender, System.EventArgs e)
        {
            label.Text = "You've successfully joined the Serenity Crew"; 
        }
        void OnDisappearing(Object sender, System.EventArgs e)
        {
            DisplayAlert("Serenity", "You are leaving this page. Please press BYE to continue", "BYE");
        }

    }
}