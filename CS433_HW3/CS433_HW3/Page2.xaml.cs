﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CS433_HW3
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Page2 : ContentPage
	{
		public Page2 ()
		{
			InitializeComponent ();
		}


        void OnAppearing(Object sender, System.EventArgs e)
        {
            DisplayAlert("Jolly Roger", "You have selected the ship Jolly Roger! Please press OK to continue", "OK");
        }
        void Button_Clicked(Object sender, System.EventArgs e)
        {
            label.Text = "You've successfully joined the Jolly Roger Crew";
        }
        void OnDisappearing(Object sender, System.EventArgs e)
        {
            DisplayAlert("Jolly Roger", "You are leaving this page. Please press BYE to continue", "BYE");
        }
    }
}